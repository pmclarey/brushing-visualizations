var spm = function scatterPlotMatrix(data) {
    var width = 960,
            size = 230,
            padding = 20;
    
    var x = d3.scale.linear()
            .range([padding / 2, size - padding / 2]);
    
    var y = d3.scale.linear()
            .range([size - padding / 2, padding / 2]);
    
    var xAxis = d3.svg.axis()
            .scale(x)
            .orient('bottom')
            .ticks(6);
    
    var yAxis = d3.svg.axis()
            .scale(y)
            .orient('left')
            .ticks(6);
    
    var color = d3.scale.linear()
            .domain([40, 65,90])
            .range(['#ffeda0', '#feb24c', '#f03b20']);
    
    draw();
    
    function draw() {
        var domainByTrait = {},
                traits = d3.keys(data[0]).filter(function(d) {
                    return d === 'Atmospheric Pressure' 
                            || d === 'Wind Speed'
                            || d === 'Dew Point'
                            || d === 'Sky Cover';
                }),
                n = traits.length;
        
        traits.forEach(function(trait) {
            domainByTrait[trait] = d3.extent(data, function(d){
                return +d[trait];
            });
        });
        
        xAxis.tickSize(size * n);
        yAxis.tickSize(-size * n);
        
        var brush = d3.svg.brush()
                .x(x)
                .y(y)
                .on('brushstart', brushstart)
                .on('brush', brushmove)
                .on('brushend', brushend);
        
        var svg = d3.select('#scatter-plot-matrix').append('svg')
                .attr('width', size * n + padding)
                .attr('height', size * n + padding)
            .append('g')
                .attr('transform', 'translate(' + padding + ','
                        + padding / 2 + ')');
        
        svg.selectAll('.x.axis')
                .data(traits)
            .enter().append('g')
                .attr('class', 'x axis')
                .attr('transform', function(d, i) {
                    return 'translate(' + (n - i - 1) * size + ',0)';
                })
                .each(function(d) {
                    x.domain(domainByTrait[d]);
                    d3.select(this).call(xAxis);
                });
                
        svg.selectAll('.y.axis')
                .data(traits)
            .enter().append('g')
                .attr('class', 'y axis')
                .attr('transform', function(d, i) {
                    return 'translate(0,' + i * size + ')';
                })
                .each(function(d) {
                    y.domain(domainByTrait[d]);
                    d3.select(this).call(yAxis);
                });
        
        var cell = svg.selectAll('.cell')
                .data(cross(traits, traits))
            .enter().append('g')
                .attr('class', 'cell')
                .attr('transform', function(d) {
                    return 'translate(' + (n - d.i - 1) * size + ','
                            + d.j * size + ')';
                })
                .each(plot);
        
        cell.filter(function(d) { return d.i === d.j; }).append('text')
                .attr("x", padding)
                .attr("y", padding)
                .attr("dy", ".71em")
                .text(function(d) { return d.x; });

        cell.call(brush);
        
        function plot(p) {
            var cell = d3.select(this);

            x.domain(domainByTrait[p.x]);
            y.domain(domainByTrait[p.y]);

            cell.append('rect')
                    .attr('class', 'frame')
                    .attr('x', padding / 2)
                    .attr('y', padding / 2)
                    .attr('width', size - padding)
                    .attr('height', size - padding);

            cell.selectAll('circle')
                    .data(data)
                .enter().append('circle')
                    .attr('cx', function(d) { return x(d[p.x]); })
                    .attr('cy', function(d) { return y(d[p.y]); })
                    .attr('r', 4)
                    .style('fill', function(d) {
                        return color(d['Air Temperature']);
                    });
        }
        
        var brushCell;
        
        function brushstart(p) {
            if (brushCell !== this) {
                d3.select(brushCell).call(brush.clear());
                x.domain(domainByTrait[p.x]);
                y.domain(domainByTrait[p.y]);
                brushCell = this;
            }
        }
        
        function brushmove(p) {
            var e = brush.extent();
            svg.selectAll('circle').classed('hidden', function(d) {
                return e[0][0] > d[p.x] || d[p.x] > e[1][0]
                        || e[0][1] > d[p.y] || d[p.y] > e[1][1];
            });
        }
        
        function brushend() {
            if (brush.empty()) {
                svg.selectAll('.hidden').classed('hidden', false);
            }
        }
        
    };
};

var pc = function ParallelCoord(data) {
    var margin = {top: 30, right: 10, bottom: 10, left: 10},
            width = 1920 - margin.left - margin.right,
            height = 1200 - margin.top - margin.bottom;
    
    var x = d3.scale.ordinal().rangePoints([0, width], 1),
            y = {},
            dragging = {};
    
    var line = d3.svg.line();
    var axis = d3.svg.axis().orient("left");
    var dimensions;
    var foreground;
    var color = d3.scale.linear()
            .domain([40, 65,90])
            .range(['#ffeda0', '#feb24c', '#f03b20']);
    
    var svg = d3.select("#parallel-coord").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var rows = data;
    
    x.domain(dimensions = d3.keys(rows[0]).filter(function(d) {
        if (d === 'Atmospheric Pressure' 
                || d === 'Wind Speed'
                || d === 'Dew Point'
                || d === 'Sky Cover') {
            
            y[d] = d3.scale.linear()
                    .domain(d3.extent(rows, function(p) { return +p[d]; }))
                    .range([height, 0]);
            return true;
        } else {
            return false;
        }
    }));
        
    var background = svg.append("g")
            .attr("class", "background")
            .selectAll("path")
            .data(rows)
        .enter().append("path")
            .attr("d", path);
        
    var foreground = svg.append("g")
            .attr("class", "foreground")
            .selectAll("path")
            .data(rows)
        .enter().append("path")
            .attr("d", path)
            .style('stroke', function(d) {
                return color(d['Air Temperature']);
            });
        
    // Add a group element for each dimension.
    var g = svg.selectAll(".dimension")
            .data(dimensions)
        .enter().append("g")
            .attr("class", "dimension")
            .attr("transform", function(d) { return "translate(" + x(d) + ")"; })
            .call(d3.behavior.drag()
            .origin(function(d) { return {x: x(d)}; })
            .on("dragstart", function(d) {
                dragging[d] = x(d);
                background.attr("visibility", "hidden");
            })
            .on("drag", function(d) {
                dragging[d] = Math.min(width, Math.max(0, d3.event.x));
                foreground.attr("d", path);
                dimensions.sort(function(a, b) { return position(a) - position(b); });
                x.domain(dimensions);
                g.attr("transform", function(d) { return "translate(" + position(d) + ")"; });
            })
            .on("dragend", function(d) {
                delete dragging[d];
                transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
                transition(foreground).attr("d", path);
                background
                        .attr("d", path)
                        .transition()
                        .delay(500)
                        .duration(0)
                        .attr("visibility", null);
            }));
        
    // Add an axis and title.
    g.append("g")
            .attr("class", "axis")
            .each(function(d) { d3.select(this).call(axis.scale(y[d])); })
        .append("text")
            .style("text-anchor", "middle")
            .attr("y", -9)
            .text(function(d) { return d; });
    
    // Add and store a brush for each axis.
    g.append("g")
            .attr("class", "brush")
            .each(function(d) {
                d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
            })
            .selectAll("rect")
            .attr("x", -8)
            .attr("width", 16);
    
    function position(d) {
        var v = dragging[d];
        return v == null ? x(d) : v;
    }

    function transition(g) {
        return g.transition().duration(500);
    }

    function path(d) {
        return line(dimensions.map(function(p) {
            return [position(p), y[p](d[p])];
        }));
    }

    function brushstart() {
        d3.event.sourceEvent.stopPropagation();
    };

    function brush() {
        var actives = dimensions.filter(function(p) {
            return !y[p].brush.empty();
        }),
        extents = actives.map(function(p) {
            return y[p].brush.extent();
        });
        foreground.style("display", function(d) {
            return actives.every(function(p, i) {
                return extents[i][0] <= d[p] && d[p] <= extents[i][1];
            }) ? null : "none";
        });
    };
};

function cross(a, b) {
    var c = [], n = a.length, m = b.length, i, j;
    for (i = -1; ++i < n;)
        for (j = -1; ++j < m;)
            c.push({x: a[i], i: i, y: b[j], j: j});
    return c;
}

window.onload = function() {
    d3.csv('./data/data.csv', function(error, data) {
        if (error) throw error;
        spm(data);
        pc(data);
    });
};